import cv2 as cv

def resize_image(frame,size,mode):
    if mode == 1:
        #最临近区域插值
        frame = cv.resize(frame,size,interpolation=cv.INTER_NEAREST)#计算速度最快
    if mode == 2:
        #线性插值
        frame = cv.resize(frame,size,interpolation=cv.INTER_LINEAR)#计算速度快
    if mode == 3:
        #使用双三次插值算法
        frame = cv.resize(frame,size,interpolation=cv.INTER_CUBIC)#放大图像适合
    if mode == 4:
        #使用区域插值算法
        frame = cv.resize(frame,size,interpolation=cv.INTER_AREA)#缩小图像适用
    
    return frame